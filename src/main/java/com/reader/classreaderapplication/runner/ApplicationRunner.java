package com.reader.classreaderapplication.runner;

import com.reader.classreaderapplication.service.ClassReaderServiceImpl;
import org.springframework.boot.CommandLineRunner;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Objects;
import java.util.Scanner;

public class ApplicationRunner implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {

        ClassReaderServiceImpl classReaderService = new ClassReaderServiceImpl();
        System.out.println("************* Welcome to Class Reader Application **********");

        System.out.println("Please Enter the Class Name:");
        Scanner sc= new Scanner(System.in);
        String className = sc.nextLine();

        Class classInstance = createInstanceOfClass(className);

        if(Objects.isNull(classInstance) ) {
            System.out.println("Provided Class is not a valid class");
        } else {
            invokeSelectOptions(classInstance,classReaderService);
        }
        sc.close();

    }

    private void invokeSelectOptions(Class classInstance,ClassReaderServiceImpl classReaderService) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        System.out.println("!!! Select the Menu Options !!!");
        System.out.println("-------------------------------");
        System.out.println("1.Methods");
        System.out.println("2.Class");
        System.out.println("3.SubClasses");
        System.out.println("4.Parent Classes");
        System.out.println("5.Constructors");
        System.out.println("6.Data Members");
        Scanner sc2 = new Scanner(System.in);
        Integer option = sc2.nextInt();

        switch (option) {
            case 1:
                classReaderService.getMethods(classInstance);
                invokeOtherInformation(classInstance,classReaderService);
                break;
            case 2:
                classReaderService.getClasses(classInstance);
                invokeOtherInformation(classInstance,classReaderService);
                break;
            case 3:
                classReaderService.getSubClasses(classInstance);
                invokeOtherInformation(classInstance,classReaderService);
                break;
            case 4:
                classReaderService.getParentClasses(classInstance);
                invokeOtherInformation(classInstance,classReaderService);
                break;
            case 5:
                classReaderService.getConstructors(classInstance);
                invokeOtherInformation(classInstance,classReaderService);
                break;
            case 6:
                classReaderService.getDataMembers(classInstance);
                invokeOtherInformation(classInstance,classReaderService);
                break;
            default:
                break;

        }
    }

    private void invokeOtherInformation(Class classInstance,ClassReaderServiceImpl classReaderService) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        System.out.println("!!! Do you want to see any other information ? !!!");
        System.out.println(" Press yes to recheck the menu and No if you want to continue ");
        System.out.println("-------------------------------");

        Scanner sc3 = new Scanner(System.in);
        String option = sc3.nextLine();

        if("yes".equalsIgnoreCase(option)) {
            invokeSelectOptions(classInstance,classReaderService);
        } else if("no".equalsIgnoreCase(option)) {
            viewOtherOptions(classInstance,classReaderService);
            invokeOtherInformation(classInstance,classReaderService);
        } else {
            System.out.println("Entered Invalid Options  please choose Yes or No !!");
            invokeOtherInformation(classInstance,classReaderService);
        }

    }

    private void viewOtherOptions(Class classInstance, ClassReaderServiceImpl classReaderService) throws ClassNotFoundException, InstantiationException, IllegalAccessException {

        System.out.println("1.Store Information into file");
        System.out.println("2.To See all Previous files created");
        System.out.println("3.Exit without storing");



        Scanner sc4 = new Scanner(System.in);
        Integer option = sc4.nextInt();

        switch (option) {
            case 1:
                classReaderService.StoreInformationIntoFile(classInstance);
                invokeOtherInformation(classInstance,classReaderService);
                break;
            case 2:
                classReaderService.ViewAllPreviousFiles(classInstance);
                invokeOtherInformation(classInstance,classReaderService);
                break;
            case 3:
                System.out.println("Quitting from application");
                break;
            default:
                break;

        }
    }

    Class createInstanceOfClass(String className) throws ClassNotFoundException, InstantiationException, IllegalAccessException{
        Class classTemp = null;
        try{
            if(className.matches(".*\\d.*")){
                System.out.println("Please enter the valid class name which contains numbers");
            } else{
                classTemp = Class.forName(className);
                System.out.println(classTemp);
            }
        } catch (Exception exception) {
            System.out.println("Please provide the validate class name" + exception);
        }
        return classTemp;
    }
}

