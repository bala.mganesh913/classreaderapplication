package com.reader.classreaderapplication.service;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;
import sun.reflect.ReflectionFactory;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class ClassReaderServiceImpl {

    public void getMethods(Class classInstance) {
        for (Method method : classInstance.getDeclaredMethods()) {
                System.out.println("Methods- > "+method.getName());
        }
    }

    public void getClasses(Class classInstance) {
            System.out.println("You are viewing current Class name-> " +classInstance.getName());
    }

    public void getSubClasses(Class classInstance) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
        System.out.println("You are viewing Sub Class name-> " +classInstance.asSubclass(classInstance));

    }

    public void getParentClasses(Class classInstance) {

        String parentClass = classInstance.getClass().getSuperclass().getName();
        System.out.println("Parent -> " +parentClass);

    }

    public void getConstructors(Class classInstance) {

        Constructor constructors[] = classInstance.getConstructors();
        for(Constructor constructor: constructors) {
            System.out.println("Constructor -> "  +constructor);
        }
    }

    public void getDataMembers(Class classInstance) {

        Field[] fields = classInstance.getDeclaredFields();
        for (Field field : fields) {
            System.out.println("Data Members ->"+field.getName());
        }
    }

    public void StoreInformationIntoFile(Class classInstance) {

        try{
            String fileName = new SimpleDateFormat("yyyyMMddHHmm'.txt'").format(new Date());
            PrintStream out = new PrintStream(new FileOutputStream("output"+fileName));
            System.setOut(out);
        } catch (Exception exception) {
            System.out.println("Exception in writing the file" + exception);
        }
    }

    public void ViewAllPreviousFiles(Class classInstance) {
        File folder = new File(".");

        String[] files = folder.list();

        for (String file : files)
        {
            if(file.startsWith("output")) {
                System.out.println("Files - > " +file);
        }
        }
    }
}
