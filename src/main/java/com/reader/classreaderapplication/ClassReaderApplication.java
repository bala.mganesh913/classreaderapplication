package com.reader.classreaderapplication;

import com.reader.classreaderapplication.runner.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.reader.classreaderapplication.runner"})
public class ClassReaderApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClassReaderApplication.class, args);
    }

    @Bean
    public ApplicationRunner schedulerRunner() {
        return new ApplicationRunner();
    }
}
